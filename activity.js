console.log("Hello World")

class Student {
    constructor(name,email,grades){
        this.name = name;
        this.email = email;
        this.gradeAve = undefined;
        this.passed = undefined
        this.passedWithHonors = undefined
        if(Array.isArray(grades) && grades.length === 4){
            (grades.every(num => (num >= 0 && num <= 100)))? this.grades = grades : this.grades = undefined
        }
        else this.grades = undefined;
    }
    login(){
        console.log(`${this.email} has logged in`)
        return this
    }

    logout(){
        console.log(`${this.email} has logged out`)
        return this
    }

    listGrades(){
        console.log(`${this.name}'s quarterly grades are: ${this.grades}`)
        return this
    }
    computeAve() {
        this.gradeAve = (this.grades.reduce((a, b) => a + b) / this.grades.length)
        return this
    }
    willPass() {
        this.computeAve()
        this.passed = this.gradeAve >= 85
        return this
    }
    willPassWithHonors() {
        this.willPass()
        if(this.passed){
            if (this.gradeAve >= 90){
                this.passedWithHonors = true
            }else {
                this.passedWithHonors = false;
            }    
        } else this.passedWithHonors = undefined
        return this
    }
}

class Section {
    constructor(name){
        this.name = name;
        this.students = []
        this.honorStudents = undefined
        this.honorsPercentage = undefined
        this.honorStudentsInfo = []
    }

    addStudent(name,email,grades){
        this.students.push(new Student(name,email,grades))
        return this
    }

    countHonorStudents(){
        let count = 0
        this.students.forEach(student => {
            student.willPassWithHonors()
            if(student.passedWithHonors) count++
        })
        this.honorStudents = count;
        return this
    }

    countHonorPercentage(){
        this.countHonorStudents()
        this.honorsPercentage = this.honorStudents/this.students.length*100
        return this
    }

    retrieveHonorStudentInfo(){
        this.students.forEach(student => {
            student.willPassWithHonors()
            student.computeAve()
            if (student.passedWithHonors) {
                this.honorStudentsInfo.push({
                    avgGrade: student.gradeAve,
                    email: student.email
                })
            }
        })
        return this
    }

    sortHonorStudentsByGradesDesc(){
        this.retrieveHonorStudentInfo()
        this.honorStudentsInfo = this.honorStudentsInfo.sort((a,b) => b.avgGrade - a.avgGrade)
        console.log(this.honorStudentsInfo)
        return this
    }
}

// 1. Define a Grade class whose constructor will accept a number argument to serve as its grade level. It will have the following properties:
// level initialized to passed in number argument
// sections initialized to an empty array
// totalStudents initialized to zero
// totalHonorStudents initialized to zero
// batchAveGrade set to undefined
// batchMinGrade set to undefined
// batchMaxGrade set to undefined
class Grade {
    constructor(number){
        if(typeof number !== "number") return console.log("Grade level must be a number")
        this.level = number
        this.totalStudents = 0;
        this.totalHonorStudents = 0;
        this.batchMinGrade = undefined
        this.batchMaxGrade = undefined
        this.batchAveGrade = undefined
        this.sections = []
    }
    // 2.Define an addSection() method that will take in a string argument to instantiate a new Section object and push it into the sections array property.
    addSection(name){
        if(typeof name !== "string") return console.log("Section name must be string")
        this.sections.push(new Section(name))
        return this
    }
    // 4. Define a countStudents() method that will iterate over every section in the grade level, incrementing the totalStudents property of the grade level object for every student found in every section.
    countStudents(){
        let count = 0
        this.sections.forEach(section => {
            count += section.students.length
        })
        this.totalStudents = count
        return this
    }
    // 5. Define a countHonorStudents() method that will perform similarly to countStudents() except that it will only consider honor students when incrementing the totalHonorStudents property.
    countHonorStudents(){
        let count = 0
        this.sections.forEach(section => {
            section.students.forEach(students => {
                students.willPassWithHonors()
                if(students.passedWithHonors) count++
            })
        })
        this.totalHonorStudents = count
        return this
    }
    // 6. Define a computeBatchAve() method that will get the average of all the students' grade averages and divide it by the total number of students in the grade level. The batchAveGrade property will be updated with the result of this operation.
    computeBatchAve(){
        this.countStudents()
        let total = 0
        this.sections.forEach(section => {
            section.students.forEach(students => {
                students.grades.forEach(grade => total += grade)
            })
        })
        this.batchAveGrade = total/(this.totalStudents*4)
        return this
    }
    // 7. Define a method named getBatchMinGrade() that will update the batchMinGrade property with the lowest grade scored by a student of this grade level regardless of section.
    getBatchMinGrade(){
        this.countStudents()
        let lowest = 100
        this.sections.forEach(section => {
            section.students.forEach(students => {
                students.grades.forEach(grade => {
                    if(grade < lowest) lowest = grade 
                })
            })
        })
        this.batchMinGrade = lowest
        return this
    }

    // 8. Define a method named getBatchMaxGrade() that will update the batchMaxGrade property with the highest grade scored by a student of this grade level regardless of section.
    getBatchMaxGrade(){
        this.countStudents()
        let highest = 0
        this.sections.forEach(section => {
            section.students.forEach(students => {
                students.grades.forEach(grade => {
                    if(grade > highest) highest = grade 
                })
            })
        })
        this.batchMaxGrade = highest
        return this
    }    

}
// 3.Write a statement that will add a student to section1A. The student is named John, with email john@mail.com, and grades of 89, 84, 78, and 88.
    const grade1 = new Grade(1)
    grade1.addSection("section1A")
    grade1.sections.find(section => section.name === "section1A").addStudent("John","john@mail.com",[89,84,78,88])
    grade1.sections.find(section => section.name === "section1A").addStudent("Jake","jake@mail.com",[80,86,93,76])
    grade1.sections.find(section => section.name === "section1A").addStudent("Mike","mike@mail.com",[84,89,88,88])
    grade1.sections.find(section => section.name === "section1A").addStudent("Susie","susie@mail.com",[98,99,98,87])

    grade1.addSection("section1B")
    grade1.sections.find(section => section.name === "section1B").addStudent("Joe","joe@mail.com",[93,82,79,90])
    grade1.sections.find(section => section.name === "section1B").addStudent("Mary","mary@mail.com",[78,78,79,85])
    grade1.sections.find(section => section.name === "section1B").addStudent("Jane","jane@mail.com",[87,89,91,93])
    grade1.sections.find(section => section.name === "section1B").addStudent("Jonas","jonas@mail.com",[78,82,79,85])

    grade1.addSection("section1C")
    grade1.sections.find(section => section.name === "section1C").addStudent("Chito","chito@mail.com",[85,85,90,90])
    grade1.sections.find(section => section.name === "section1C").addStudent("Kiko","francism@mail.com",[87,83,91,90])
    grade1.sections.find(section => section.name === "section1C").addStudent("Gloc-9","walangapelyido@mail.com",[85,85,85,85])
    grade1.sections.find(section => section.name === "section1C").addStudent("Rico","rico@mail.com",[87,89,91,93])

    grade1.addSection("section1D")
    grade1.sections.find(section => section.name === "section1D").addStudent("Eheads","eheads@mail.com",[83,85,85,85])
    grade1.sections.find(section => section.name === "section1D").addStudent("Parokya","parokya@mail.com",[90,89,95,95])
    grade1.sections.find(section => section.name === "section1D").addStudent("Kamikazee","kamikazee@mail.com",[83,85,85,85])
    grade1.sections.find(section => section.name === "section1D").addStudent("Orange&Lemons","orangelemons@mail.com",[83,85,85,85])







/* 
const section1A = new Section("section1A")
section1A.addStudent("John","john@mail.com",[89,84,78,88])
section1A.addStudent("Joe","joe@mail.com",[78,82,79,85])
section1A.addStudent("Jane","jane@mail.com",[87,89,91,93])
section1A.addStudent("Jessie","jessie@mail.com",[91,89,92,93]) */
